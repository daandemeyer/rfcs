DOC_FILES=$(wildcard rfcs/*.rst) $(wildcard *.rst)

all:

check: lint

lint:
	rstcheck --report-level warning $(DOC_FILES)
